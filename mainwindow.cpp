#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDebug>

MainWindow::MainWindow(const QStringList &stringList, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    m_model(stringList)
{
    ui->setupUi(this);
    ui->stringListView->setModel(&m_model);
    connect(ui->stringListView, &QListView::clicked, this, &MainWindow::rowChanged);
    connect(ui->addStringPushButton, &QAbstractButton::clicked, this, &MainWindow::onAddString);
    connect(ui->removeStringPushButton, &QAbstractButton::clicked, this, &MainWindow::onRemoveString);
    m_selectedRow = 0;
}

QListView* MainWindow::getListView()
{
    return ui->stringListView;
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::onAddString()
{
    auto  stringCount = ui->stringCountSpinBox->value();
    m_model.insertRows(m_selectedRow, stringCount);
}

void MainWindow::onRemoveString()
{
    auto  stringCount = ui->removeStringCountSPinBox->value();
    m_model.removeRows(m_selectedRow, stringCount);
}

void MainWindow::rowChanged(QModelIndex index)
{
    m_selectedRow = index.row();
}
